<?php
namespace Parkingmap\Wrapper\Service;

use Parkingmap\Wrapper\Event\EventInterface;
use Parkingmap\Wrapper\Event\Events;
use Parkingmap\Wrapper\Item\ItemInterface;
use Parkingmap\Wrapper\Store\DataStoreInterface;

interface ServiceInterface
{
    public function __construct(array $config, DataStoreInterface $datastore);
    public function run(EventInterface $event, ItemInterface $item): Events;
}