<?php


namespace Parkingmap\Wrapper\Store;

use Parkingmap\Wrapper\Event\EventInterface;

interface DataStoreInterface
{
    public function search(array $params): array;

    public function save(EventInterface $event);

    public function delete(array $params);
}